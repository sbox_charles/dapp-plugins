'use strict'
const fs = require('fs').promises


let puppeteer
let appconfig
let loc
let logger
let util

/*    outcome/
 * Login as the requested user and then do whatever requested by the
 * user as long as he has sales navigator/premium enabled.
 */
async function asUser(auth, dothis, ctx) {
  let cfg = appconfig.get()
  const browser = await puppeteer.launch(puppetArgs(cfg))
  const page = await browser.newPage()

  page.setCacheEnabled(false)
  
  if(cfg.puppetTimeout) {
    process.env.PUPPET_TIMEOUT = cfg.puppetTimeout
    await page.setDefaultTimeout(cfg.puppetTimeout)
  } else {
    if(!process.env.PUPPET_TIMEOUT) process.env.PUPPET_TIMEOUT = 30000
    await page.setDefaultTimeout(process.env.PUPPET_TIMEOUT)
  }
  
  // for debugging uncomment below
  //page.on('console', msg => console.log(msg.text()))

  let haderror

  try {
    await page.setViewport({ width: 1920, height: 1080 })
    let loggedin = await cookie_login_1(page, auth)
    if(!loggedin) {
      await auth_login_1(auth, page)
      await save_login_cookie_1(page, auth)
    }
    /*  randomly scroll around sometimes */
    let call = Math.floor(Math.random() * 5)
    if(call == 8) await autoScroll(page)

    if(!auth.allowfreeuser) {
      await check_premium_enabled_1(page)
    }

    await dothis(browser, page, ctx)

  } catch(e) {
    try {
      await page.waitForSelector('#captcha-internal')
      const captcha = await page.evaluate(async () =>{
        return document.getElementById('captcha-internal')
      })
      if(captcha) {
        logger.log(`Warning: Captcha`)
        await page.waitFor('input[role=combobox]', { timeout: 200000 })
      } else {
        haderror = e
      }
    } catch(err){      
      haderror = e
    }
  }

  if(haderror && !cfg.noDmp) {
    let dmp = loc.dmp()
    let content = await page.content()
    logger.log(`Writing dump file: ${dmp}`)
    await fs.writeFile(dmp, content)
  }
  await browser.close()
  if(haderror) throw haderror

  /*    outcome/
   * We check if we have an element (li-icon) with type
   * "premium-wordmark" which would indicate that we have premium
   * enabled. Otherwise we check if we have a li-icon with type
   * "sales-navigator-app-icon" which would indicate we have a Sales
   * Navigator subscription.
   */
  async function check_premium_enabled_1(page) {
    let enabled = await page.evaluate(async () => {
      let elems = document.getElementsByTagName('li-icon')
      for(let i = 0;i < elems.length;i++) {
        let t = elems[i].getAttribute('type')
        if(t == 'premium-wordmark' ||
            t == 'sales-navigator-app-icon') return true
      }
    })
    if(!enabled) throw 'You need a Sales Navigator or Premium account to perform tasks on LinkedIn'
  }

  async function cookie_login_1(page, auth) {
    try {
      let cookie_f = loc.cookieFile(auth.username)
      let cookie_s = await fs.readFile(cookie_f)
      let cookies = JSON.parse(cookie_s)
      await page.setCookie(...cookies)

      await page.goto('https://www.linkedin.com/')
      await page.waitFor('input[role=combobox]')

      return true

    } catch(e) {
      return false
    }
  }

  async function save_login_cookie_1(page, auth) {
    try {
      let cookie_f = loc.cookieFile(auth.username)
      let cookies = await page.cookies()
      await fs.writeFile(cookie_f, JSON.stringify(cookies))
    } catch(e) {
      logger.error(e)
    }
  }

  async function auth_login_1(auth, page) {
    if(!auth.username || !auth.password) throw "LinkedIn username/password not found"

    await page.goto('https://www.linkedin.com/uas/login')

    const user_name = "input#username"
    await page.waitForSelector(user_name)
    await page.type(user_name, auth.username)

    const pass_word = "input#password"
    await page.waitForSelector(pass_word)
    await page.type(pass_word, auth.password)

    const submitButton = "button.btn__primary--large"
    await page.waitForSelector(submitButton)
    await page.click(submitButton)

    await page.waitFor('input[role=combobox]',{timeout:200000})
  }
}

/*    outcome/
 * View the given url on linkedin as the authorized user
 */
async function view(auth, url) {

  await asUser(auth, async (browser, page) => {
    await page.goto(url)
    await page.waitFor('input[role=combobox]')
    let currentUrl = page.url()
    if(currentUrl.includes('unavailable')) throw new ProfileUnavailableError(`${url} this profile is not available`)
    await autoScroll(page)
    await autoScroll(page)
  })
}

const MAX_PENDING_CONNECTIONS=1000
/*    outcome/
 * If we are connected to the given user then we are completed.
 * Otherwise, make a connection request if we have not already done so
 * (and if we don't have too many connection requests pending).
 */
async function connect(auth, url, note, email) {
  let ctx = {}
  await asUser(auth, async (browser, page, ctx) => {
    await page.goto(url)
    try {

      try {
        await page.waitForSelector('.distance-badge.separator')

        let connected = await page.evaluate(async () => {
          let d = document.getElementsByClassName("distance-badge separator")[0]
          let txt = d.innerText
          return txt.indexOf("1st degree connection") != -1
        })
        if(connected) {
          ctx.status = "completed"
          return
        }
      } catch(e) {
        /* ignore - we are not connected */
      }


      try {
        await page.goto("https://www.linkedin.com/mynetwork/invitation-manager/sent/")
        await page.waitForSelector('.mn-invitation-list')

        await autoScroll(page)
       
        let status = await page.evaluate(async ({ url, MAX_PENDING_CONNECTIONS }) => {
          /**
          *  outcome/
          * This function removes the domain name from absolute_url and compare with relate_url.
          * if path's are matching return true else false
          * 
          * @param {*} url1 
          * @param {*} url2 
          */
          function compareURLResourcePath(url1, url2){
            if (!url1 || !url2) return false

            if(isAbsoulteURL(url1)) 
                url1 = new URL(url1).pathname
        
            if(isAbsoulteURL(url2))
                url2 = new URL(url2).pathname
        
            if(!url1.endsWith('/')) url1 += '/'
            if(!url2.endsWith('/')) url2 += '/'
            if(!url1.startsWith('/')) url1 = '/' + url1 
            if(!url2.startsWith('/')) url2 = '/' + url2 
            
            return url1 == url2;
          }

          function isAbsoulteURL(url) {
            try{
                new URL(url)
                return true
            }catch(e) {
                return false;
            }
            
        }
          let invites = document.getElementsByClassName("mn-invitation-list")[0]
          let urls = invites.getElementsByTagName('a')
          for(let i = 0;i < urls.length;i++) {
            let isConnectPending = compareURLResourcePath(url, urls[i].href)
            if(isConnectPending) {
              return 'inprogress'
            }
          }

          let filterbar = document.getElementsByClassName('mn-filters-toolbar')[0]
          let nums = filterbar.innerText
          let m = nums.match(/People \((.*?)\)/)
          if(m) {
            let num = parseInt(m[1])
            if(!isNaN(num)) {
              if(num > MAX_PENDING_CONNECTIONS) return 'toomany'
            }
          }

        }, { url, MAX_PENDING_CONNECTIONS })
        if(status) {
          ctx.status = status
          return
        }
      } catch(e) {
        if(e.name != 'TimeoutError') throw e
        /* if no pending invitations - we can continue */
      }

      await page.goto(url)
      let currentUrl = page.url()
      if(currentUrl.includes('unavailable')) throw new ProfileUnavailableError(`${url} this profile is not available`)
      await waitForConnectionOption(page)
      
      await page.click('.pv-s-profile-actions--connect')
      try {
        await page.waitForSelector(
          '[aria-label="Send now"]',
          { visible: true })
      } catch(e) {
        /* try another interface */
        await page.waitForSelector(
          '[aria-label="Send invitation"]',
          { visible: true })
      }

      if(note) {
        await waitForNoteOption(page,note)
        await waitforemailoption(page,email,ctx)
        await clickButton([
          'Done',
          'Send now',
          'Send invitation',
        ], page)
      } else {
        await waitforemailoption(page,email,ctx)
        await clickButton([
          'Done',
          'Send now',
          'Send invitation',
        ], page)
      }

      ctx.status = 'inprogress'

    } catch(e) {
      ctx.err = e
      await browser.close()
      throw e
    }
  }, ctx)
  return ctx
}

function clickButton(names, page) {
  let promises = names.map(n => page.click(`button[aria-label="${n}"]`))
  return new Promise((resolve, reject) => {
    Promise.allSettled(promises).then(v => {
      for(let i = 0;i < v.length;i++) {
        if(v[i].status === 'fulfilled') return resolve()
      }
      return reject(v[0].reason)
    })
    .catch(reject)
  })
}

async function checkConnect(auth, url){
  let ctx = {}
  await asUser(auth, async (browser, page, ctx) => {
    await page.goto(url)

    try {
      await page.waitForSelector('.distance-badge.separator')

      let connected = await page.evaluate(async () => {
        let d = document.getElementsByClassName("distance-badge separator")[0]
        let txt = d.innerText
        return txt.indexOf("1st degree connection") != -1
      })
      if(connected) {
        ctx.status = "completed"
        return
      } else {
        ctx.status = "inprogress"  
      }
    } catch(e) {
      ctx.status = "inprogress"
    }
    
  }, ctx)
  console.log(ctx)
  return ctx
    
}
// For Handling Email prompts while sending connection request
async function waitforemailoption(page,email,ctx){
  try{
    const emailid = "input#email"
    await page.waitForSelector(emailid,{visible:true})
    if(email){
        await page.type(emailid,email)
    }
}catch(e){
    ctx.status = 'inprogress'        
}
}

async function waitForNoteOption(page,note){
  try{
      // replaced the extra space with single space
      note = note.replace(/\r\n/g, "\n");
      await page.waitForSelector('button[aria-label="Add a note"]', {visible: true})
      await page.click('button[aria-label="Add a note"]')
      await page.$('#custom-message')
            .then((connectionNote) => connectionNote.type(note))  
    }catch(e){
      await page.$('#custom-message')
        .then((connectionNote) => connectionNote.type(note)) 
        }
   }    
async function waitForConnectionOption(page) {
  try{
    await page.waitForSelector(
      '.pv-s-profile-actions--connect',
      { visible: true })
  } catch(e) {
    await page.click('.pv-s-profile-actions__overflow-toggle')
  }

}

/*    outcome/
 * Find the matching invitation and click on the 'Withdraw' button.
 */
async function disconnect(auth, url) {
  let ctx = {}
  await asUser(auth, async (brower, page, ctx) => {
    try {

      await page.goto("https://www.linkedin.com/mynetwork/invitation-manager/sent/")

      await page.waitForSelector('.mn-invitation-list')

      await autoScroll(page)

      let clicked = await page.evaluate(async (url) => {
        let invites = document.getElementsByClassName("mn-invitation-list")[0]
        invites = invites.getElementsByTagName('li')
        for(let i = 0;i < invites.length;i++) {
          let invite = invites[i]
          let urls = invite.getElementsByTagName('a')
          for(let i = 0;i < urls.length;i++) {
            if(url == urls[i]) {
              let btn = invite.getElementsByTagName('button')[0]
              btn.click()
              return true
            }
          }
        }
      }, url)

      if(clicked) {
        await page.click('.artdeco-modal__confirm-dialog-btn.artdeco-button.artdeco-button--2.artdeco-button--primary.ember-view')
        await page.waitFor("p.artdeco-toast-item__message")
      } else {
        ctx.disconnected = true
        ctx.err = 'No invite found to withdraw'
      }

    } catch(e) {
      ctx.err = e
      await browser.close()
      throw e
    }
  }, ctx)

  return ctx
}

/*    outcome/
 * Click the 'message' button and send a message to the user
 */
async function message(auth, url, msg) {
  let ctx = {}
  // replaced the extra space with single space
  msg = msg.replace(/\r\n/g, "\n");
  await asUser(auth, async (browser, page, ctx) => {
    try {
      await page.goto(url)
      await page.waitFor(2000)
      let name = await page.evaluate(()=>{
      let nametext = document.querySelector('li.inline.t-24.t-black.t-normal.break-words').innerText 
      return nametext
      })
      await page.goto('https://www.linkedin.com/messaging/')
      await page.waitFor(5000)
      let convlist = await page.evaluate(()=>{
      let msgcont = document.querySelectorAll('h3.msg-conversation-listitem__participant-names')
      let leadlist = []
      for(let i = 0;i<msgcont.length;i++){
          let msgUsers = msgcont[i].innerText
          leadlist.push(msgUsers)
      }
      return leadlist
      })
      if(name && convlist){
          let convVar
          convlist.forEach((ele)=>{
              if(ele === name) convVar= true 
              },name)
          
          if(convVar){
              let idx = convlist.indexOf(name)
              await page.evaluate((idx)=>{
              let recipients = document.querySelectorAll('h3.msg-conversation-listitem__participant-names')
              recipients[idx].click()
              },idx)
              await page.waitFor(5000)
              let msgList = await page.evaluate(()=>{
                  let msgs = document.querySelectorAll(".msg-s-event-listitem__body")
                  let msg_list = []
                  for (let j =0;j<msgs.length;j++){
                      msg_list.push(msgs[j].innerText)
                  }
                  return msg_list
                  
                })
            if(msgList){      
                  let msgSentAlready 
                  for (let k =0;k<msgList.length;k++){
                  
                      if(util.compareTwoStrings(msgList[k].toLowerCase(), msg.toLowerCase()) > 0.9) {
                          logger.log(`(${msg}) This msg was already sent.`)
                          msgSentAlready =  true
                          ctx.status = "completed"
                          ctx.reason = "msg already sent"
                          break
                      }else{
                        msgSentAlready = false
                      }
                  }                    
                  if(!msgSentAlready){
            
                      await page.goto(url)
                      await page.waitFor('input[role=combobox]')
                
                      await page.evaluate(async () => {
                        let as = document.getElementsByTagName('a')
                        let found
                        for(let i = 0;i < as.length;i++) {
                          let curr = as[i]
                          if(curr.href
                            && curr.href.match(/messaging\/thread\/.*compose_message_button/)) {
                            found = curr
                            break
                          }
                        }
                        if(!found) throw('Failed to find message button')
                        found.click()
                      })
                      let msgbox_sel = 'div.msg-form__contenteditable[contenteditable=true]'
                      await page.waitFor(msgbox_sel)
                      await page.type(msgbox_sel, msg)
                
                      await page.click('button.msg-form__send-button')
                      await page.waitFor(1000)
                      ctx.status = "completed"
                }
            }
          }
      }
    }
    catch(e) {
      console.error(e)
      ctx.err = e
      await browser.close()
      throw e
    }
  }, ctx)

  return ctx
}

/*    outcome/
 * Check if we have messages from the given user's url
 * by clicking all the available messages and seeing if
 * any of them match our user
 */
async function checkresponse(auth, url) {
  let ctx = {}
  await asUser(auth, async (browser, page, ctx) => {
    try {
      await page.goto('https://www.linkedin.com/messaging/')
      await page.waitFor('a[data-control-name=view_message]')
      let msgs = await page.evaluate(async () => {
        let cont = document.getElementsByClassName('msg-conversations-container')[0]
        let msgs = cont.querySelectorAll('a[data-control-name=view_message]')

        let hrefs = []
        for(let i = 0;i < msgs.length;i++) {
          hrefs.push(msgs[i].href)
        }
        return hrefs
      })

      for(let i = 0;i < msgs.length;i++) {
        await page.goto(msgs[i])
        await page.waitFor('a[data-control-name=view_profile]')
        let response = await page.evaluate(url => {
          let thread = document.getElementsByClassName('msg-thread')[0]
          let msgListContent = thread.getElementsByClassName('msg-s-message-list-content')[0]
          let msgList = msgListContent.getElementsByClassName('msg-s-message-list__event')
          for(let i = 0;i < msgList.length;i++) {
            
            if(compareURLResourcePath(msgList[i].querySelector('a[data-control-name=view_profile]').href, url)) {
              return {
                responded: true,
                msg: msgList[i].getElementsByClassName('msg-s-event-listitem__body')[0].innerText
              }
            }
          }

          function compareURLResourcePath(url1, url2){
            if (!url1 || !url2) return false

            if(isAbsoulteURL(url1))
                url1 = new URL(url1).pathname

            if(isAbsoulteURL(url2))
                url2 = new URL(url2).pathname

            if(!url1.endsWith('/')) url1 += '/'
            if(!url2.endsWith('/')) url2 += '/'
            if(!url1.startsWith('/')) url1 = '/' + url1

            if(!url2.startsWith('/')) url2 = '/' + url2

            return url1 == url2;
          }

          function isAbsoulteURL(url) {
            try{
                new URL(url)
                return true
            }catch(e) {
                return false;
            }

        }
          /*    outcome/
           * Compare the two URL paths by ignoring the difference
           * between http:// and https:// and by removing any trailing
           * slashes
           */
          function urleq(u1, u2) {
            return norm_1(u1) == norm_1(u2)

            function norm_1(u) {
              u = u.replace(/^http:/, 'https:')
              u = u.replace(/\/$/, '')
              return u
            }
          }


        }, url)
        if(response) {
          ctx.response = response
          break
        }
      }

    } catch(e) {
      console.error(e)
      ctx.err = e
      await browser.close()
      throw e
    }
  }, ctx)

  return ctx
}


/*    outcome/
 * Use the given profile map to fetch user profile details.
*/
async function scrape(auth, url) {

  let ctx = {}
  await asUser(auth, async (broswer, page, ctx) => {
    ctx.data = await fetch_profile_1(url, page)
  }, ctx)
  return ctx.data


  async function fetch_profile_1(url, page) {
    let map = appconfig.linkedinProfileMap()
    let processors = {
      "fullname": process_fullname,
      "location": process_location,
      "companytitle": process_companytitle,
    }

    await page.goto(url)
    await page.waitFor('input[role=combobox]')
    await autoScroll(page)

    let vals = await page.evaluate((map) => {
      let vals = {}
      for(let k in map) {
        let v = document.querySelector(map[k])
        if(v) vals[k] = v.innerText
      }
      return vals
    }, map)

    let data = {}
    for(let k in vals) {
      let v = vals[k]
      if(isEmpty(v)) continue
      let processor = processors[k]
      if(!processor) v = { k : v }
      else v = processor(v)
      data = { ...data, ...v }
    }

    let companydets = await page.evaluate(() => {
      let dets = document.querySelector('[data-control-name="background_details_company"]')
      if(dets) {
        let href = dets.href
        let name
        let markers = dets.querySelectorAll('.visually-hidden')
        for(let i = 0;i < markers.length;i++) {
          let m = markers[i]
          if(m.innerText == "Company Name") {
            name = m.nextElementSibling.innerText
          }
        }
        return { href, name }
      }
    })

    url = url + 'detail/contact-info/'
    await page.goto(url)
    await page.waitFor('.pv-profile-section__section-info.section-info')

    let moreinfo = await page.evaluate(() => {
      let r = {}
      let cnt = document.querySelector('.pv-profile-section__section-info.section-info')
      let sections = cnt.querySelectorAll('.pv-contact-info__contact-type')
      for(let i = 0;i < sections.length;i++) {
        let section = sections[i]
        let k = section.className.split(' ').filter(c => c.startsWith('ci-')).map(c => c.substring(3))
        let v = section.querySelector('.pv-contact-info__ci-container')
        if(v) v = v.innerText
        if(k == 'websites') continue
        if(k && v) { r[k] = v }
      }
      return r
    })

    data = { ...data, ...moreinfo }

    if(companydets && companydets.name) data.company = companydets.name

    if(!companydets || !companydets.href ||
        companydets.href.indexOf('/company/') == -1) return data

    await page.goto(companydets.href)
    await page.waitFor('[data-control-name="top_card_view_website_custom_cta_btn"]')

    let website = await page.evaluate(() => {
      let a = document.querySelector('[data-control-name="top_card_view_website_custom_cta_btn"]')
      if(a) return a.href
    })

    if(website) data.website = website

    return data
  }

  function process_fullname(v) {
    v = v.split(' ')
    if(v.length == 1) return { firstname: v[0] }
    else {
      let lastname = v.pop()
      let firstname = v.join(' ')
      return { firstname, lastname }
    }
  }

  function process_location(v) {
    v = v.split(', ')
    if(v.length == 3) {
      return {
        address: v.join(', '),
        city: v[0],
        state: v[1],
        country: v[2],
      }
    } else {
      return { address: v.join(', ') }
    }
  }

  function process_companytitle(v) {
    v = v.split(' at ')
    if(v.length == 1) return { title: v[0] }
    else {
      let company = v.pop()
      let title = v.join(' at ')
      return { title, company }
    }
  }

}

/*    understand/
 * Puppeteer can have some problems when launching. For example:
 *    1. When running on Windows7, the headless mode ignores the default
 *    proxy which means puppeteer cannot connect.
 *    2. When running under Linux it sometimes fails with 'no usable
 *    sandbox' when the user running it is not set up properly.
 *
 * To solve these problems we allow users to update the launch
 * configuration of puppeteer via the `config.json`. Users need to add a
 * `puppetArgs` key with the arguments needed. For example:
 *
 *    To solve (1):
 *
 *        `puppetArgs: [ '--proxy-server="direct://"', '--proxy-bypass-list=*']`
 *
 *      (refer: https://github.com/GoogleChrome/puppeteer/issues/2613)
 *
 *    To solve (2):
 *
 *        `puppetArgs: '--no-sandbox'`
 *
 * You can also set the headless mode by setting `puppetShow: true`
 *
 * Another problem we face is that when packaged in
 * electron ASAR format, puppeteer cannot start the chrome
 * executable. To solve this, we unpack the executable, and
 * if the executable path is in an asar packed format we
 * update it.
 *
 */
function puppetArgs(cfg) {
  let args = cfg.puppetArgs
  if(!args) args = []
  if(typeof args == 'string') args = [ args ]
  let headless = true
  if(cfg.puppetShow || cfg.puppetShow === undefined) headless = false
  let ret = { slowMo: 100, headless, args }
  let loc = puppeteer.executablePath()
  let rx_isasar1 = /\\app.asar\\/
  if(loc.match(rx_isasar1)) {
    loc = loc.replace(rx_isasar1, "\\app.asar.unpacked\\")
    ret.executablePath = loc
  }
  let rx_isasar2 = /\/app.asar\//
  if(loc.match(rx_isasar2)) {
    loc = loc.replace(rx_isasar2, "/app.asar.unpacked/")
    ret.executablePath = loc
  }
  return ret
}

/*    outcome/
 * Scroll down the page a bit
 */
async function autoScroll(page) {
  await page.evaluate(async () => {
    await new Promise((resolve, reject) => {
      let totalHeight = 0;
      let distance = 100;
      let maxdistance = 5000 + Math.floor(Math.random() * 10000);
      let timer = setInterval(() => {
        let scrollHeight = document.body.scrollHeight;
        window.scrollBy(0, distance);
        totalHeight += distance;
        if(totalHeight >= scrollHeight ||
          totalHeight > maxdistance){
          clearInterval(timer);
          resolve();
        }
      }, 100);
    });
  })
}

/*    outcome/
 * Check if a given value is a valid non-empty string
 */
function isEmpty(v) {
  if(!v) return true
  if(typeof v !== 'string') return true
  v = v.trim().toLowerCase()
  if(!v) return true
  if(v == 'null' || v == 'nil') return true
  return false
}

function dummy(name) {
  return async (auth, url) => {
    console.log(`Mock performing linkedin task ${name}...`)
    console.log(url)
    return { done: true }
  }
}

function ProfileUnavailableError(msg) {
  this.message = msg
  this.name ='ProfileUnavailableError'
}

function setup(deps) {
  puppeteer = deps.puppeteer
  appconfig = deps.appconfig
  loc = deps.loc
  logger = deps.logger
  util = deps.util
  return {
    view,
    connect,
    checkConnect,
    disconnect,
    scrape,
    message,
    checkresponse,
  }
}

module.exports = setup
